// ES6 module import statement
import {LitElement, html} from '@polymer/lit-element'

// definition of our simple element. no props. only render
class SimpleElement extends LitElement {
  _render(props) {
    return html`<h1>Hello world</h1><p>This is the simplest element.</p>`
  }
}

// This registers our element in the browser.
// The first argument is the name of our element. So we can use it by <simple-element></simple-element>
// The second argument is an HTMLElement Class (LitElement is a SubClass of HTMLElement)
customElements.define('simple-element', SimpleElement)
